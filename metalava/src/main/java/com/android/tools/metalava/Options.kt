/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tools.metalava

import com.android.SdkConstants
import com.android.SdkConstants.FN_FRAMEWORK_LIBRARY
import com.android.tools.lint.detector.api.isJdkFolder
import com.android.tools.metalava.cli.common.CommonOptions
import com.android.tools.metalava.cli.common.DefaultSignatureFileLoader
import com.android.tools.metalava.cli.common.ExecutionEnvironment
import com.android.tools.metalava.cli.common.IssueReportingOptions
import com.android.tools.metalava.cli.common.PreviouslyReleasedApi
import com.android.tools.metalava.cli.common.SourceOptions
import com.android.tools.metalava.cli.common.Terminal
import com.android.tools.metalava.cli.common.TerminalColor
import com.android.tools.metalava.cli.common.Verbosity
import com.android.tools.metalava.cli.common.cliError
import com.android.tools.metalava.cli.common.enumOption
import com.android.tools.metalava.cli.common.existingFile
import com.android.tools.metalava.cli.common.fileForPathInner
import com.android.tools.metalava.cli.common.map
import com.android.tools.metalava.cli.common.stringToExistingDir
import com.android.tools.metalava.cli.common.stringToExistingFile
import com.android.tools.metalava.cli.common.stringToNewDir
import com.android.tools.metalava.cli.common.stringToNewFile
import com.android.tools.metalava.cli.compatibility.CompatibilityCheckOptions
import com.android.tools.metalava.cli.compatibility.CompatibilityCheckOptions.CheckRequest
import com.android.tools.metalava.cli.lint.ApiLintOptions
import com.android.tools.metalava.cli.signature.SignatureFormatOptions
import com.android.tools.metalava.doc.ApiVersionFilter
import com.android.tools.metalava.doc.ApiVersionLabelProvider
import com.android.tools.metalava.manifest.Manifest
import com.android.tools.metalava.manifest.emptyManifest
import com.android.tools.metalava.model.AnnotationManager
import com.android.tools.metalava.model.Codebase
import com.android.tools.metalava.model.Item
import com.android.tools.metalava.model.PackageFilter
import com.android.tools.metalava.model.PackageItem
import com.android.tools.metalava.model.TypedefMode
import com.android.tools.metalava.model.annotation.DefaultAnnotationManager
import com.android.tools.metalava.model.source.DEFAULT_JAVA_LANGUAGE_LEVEL
import com.android.tools.metalava.model.source.DEFAULT_KOTLIN_LANGUAGE_LEVEL
import com.android.tools.metalava.model.text.ApiClassResolution
import com.android.tools.metalava.model.text.EmitFileHeader
import com.android.tools.metalava.model.visitors.ApiPredicate
import com.android.tools.metalava.reporter.Baseline
import com.android.tools.metalava.reporter.DefaultReporter
import com.android.tools.metalava.reporter.IssueConfiguration
import com.android.tools.metalava.reporter.Issues
import com.android.tools.metalava.reporter.Reportable
import com.android.tools.metalava.reporter.Reporter
import com.android.tools.metalava.stub.StubWriterConfig
import com.android.utils.SdkUtils.wrap
import com.github.ajalt.clikt.core.NoSuchOption
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.deprecated
import com.github.ajalt.clikt.parameters.options.multiple
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.unique
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.file
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.util.Optional
import java.util.function.Predicate
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import org.jetbrains.jps.model.java.impl.JavaSdkUtil

/**
 * A [ReadWriteProperty] that is used as the delegate for [options].
 *
 * It provides read/write methods and also a [disallowAccess] method which when called will cause
 * any attempt to read the [options] property to fail. This allows code to ensure that any code
 * which it calls does not access the deprecated [options] property.
 */
object OptionsDelegate : ReadWriteProperty<Nothing?, Options> {

    /**
     * The value of this delegate.
     *
     * Is `null` if [setValue] has not been called since the last call to [disallowAccess]. In that
     * case any attempt to read the value of this delegate will fail.
     */
    private var possiblyNullOptions: Options? = Options()

    /**
     * The stack trace of the last caller to [disallowAccess] (if any) to make it easy to determine
     * why a read of [options] failed.
     */
    private var disallowerStackTrace: Throwable? = null

    /** Prevent all future reads of [options] until the [setValue] method is next called. */
    fun disallowAccess() {
        disallowerStackTrace = UnexpectedOptionsAccess("Global options property cleared")
        possiblyNullOptions = null
    }

    override fun setValue(thisRef: Nothing?, property: KProperty<*>, value: Options) {
        disallowerStackTrace = null
        possiblyNullOptions = value
    }

    override fun getValue(thisRef: Nothing?, property: KProperty<*>): Options {
        return possiblyNullOptions
            ?: throw UnexpectedOptionsAccess("options is not set", disallowerStackTrace!!)
    }
}

/** A private class to try and avoid it being caught and ignored. */
private class UnexpectedOptionsAccess(message: String, cause: Throwable? = null) :
    RuntimeException(message, cause)

/**
 * Global options for the metadata extraction tool
 *
 * This is an empty options which is created to avoid having a nullable options. It is replaced with
 * the actual options to use, either created from the command line arguments for the main process or
 * with arguments supplied by tests.
 */
@Deprecated(
    """
    Do not add any more usages of this and please remove any existing uses that you find. Global
    variables tightly couple all the code that uses them making them hard to test, modularize and
    reuse. Which is why there is an ongoing process to remove usages of global variables and
    eventually the global variable itself.
    """
)
var options by OptionsDelegate

private const val INDENT_WIDTH = 45

const val ARG_CLASS_PATH = "--classpath"
const val ARG_SOURCE_FILES = "--source-files"
const val ARG_API_CLASS_RESOLUTION = "--api-class-resolution"
const val ARG_SDK_VALUES = "--sdk-values"
const val ARG_MERGE_QUALIFIER_ANNOTATIONS = "--merge-qualifier-annotations"
const val ARG_MERGE_INCLUSION_ANNOTATIONS = "--merge-inclusion-annotations"
const val ARG_VALIDATE_NULLABILITY_FROM_MERGED_STUBS = "--validate-nullability-from-merged-stubs"
const val ARG_VALIDATE_NULLABILITY_FROM_LIST = "--validate-nullability-from-list"
const val ARG_NULLABILITY_WARNINGS_TXT = "--nullability-warnings-txt"
const val ARG_NULLABILITY_ERRORS_NON_FATAL = "--nullability-errors-non-fatal"
const val ARG_DOC_STUBS = "--doc-stubs"
/** Used by Firebase, see b/116185431#comment15, not used by Android Platform or AndroidX */
const val ARG_PROGUARD = "--proguard"
const val ARG_EXTRACT_ANNOTATIONS = "--extract-annotations"
const val ARG_EXCLUDE_DOCUMENTATION_FROM_STUBS = "--exclude-documentation-from-stubs"
const val ARG_ENHANCE_DOCUMENTATION = "--enhance-documentation"
const val ARG_SKIP_READING_COMMENTS = "--ignore-comments"
const val ARG_MANIFEST = "--manifest"
const val ARG_MIGRATE_NULLNESS = "--migrate-nullness"
const val ARG_SUPPRESS_COMPATIBILITY_META_ANNOTATION = "--suppress-compatibility-meta-annotation"
const val ARG_APPLY_API_LEVELS = "--apply-api-levels"
const val ARG_JAVA_SOURCE = "--java-source"
const val ARG_KOTLIN_SOURCE = "--kotlin-source"
const val ARG_SDK_HOME = "--sdk-home"
const val ARG_JDK_HOME = "--jdk-home"
const val ARG_COMPILE_SDK_VERSION = "--compile-sdk-version"
const val ARG_INCLUDE_SOURCE_RETENTION = "--include-source-retention"
const val ARG_PASS_THROUGH_ANNOTATION = "--pass-through-annotation"
const val ARG_EXCLUDE_ANNOTATION = "--exclude-annotation"
const val ARG_DELETE_EMPTY_REMOVED_SIGNATURES = "--delete-empty-removed-signatures"
const val ARG_SUBTRACT_API = "--subtract-api"
const val ARG_TYPEDEFS_IN_SIGNATURES = "--typedefs-in-signatures"
const val ARG_IGNORE_CLASSES_ON_CLASSPATH = "--ignore-classes-on-classpath"
const val ARG_USE_K2_UAST = "--Xuse-k2-uast"
const val ARG_PROJECT = "--project"
const val ARG_SOURCE_MODEL_PROVIDER = "--source-model-provider"

class Options(
    private val executionEnvironment: ExecutionEnvironment = ExecutionEnvironment(),
    private val commonOptions: CommonOptions = CommonOptions(),
    private val sourceOptions: SourceOptions = SourceOptions(),
    private val issueReportingOptions: IssueReportingOptions =
        IssueReportingOptions(commonOptions = commonOptions),
    private val generalReportingOptions: GeneralReportingOptions = GeneralReportingOptions(),
    internal val configFileOptions: ConfigFileOptions = ConfigFileOptions(),
    val apiSelectionOptions: ApiSelectionOptions = ApiSelectionOptions(),
    val apiLintOptions: ApiLintOptions = ApiLintOptions(),
    private val compatibilityCheckOptions: CompatibilityCheckOptions = CompatibilityCheckOptions(),
    signatureFileOptions: SignatureFileOptions = SignatureFileOptions(),
    signatureFormatOptions: SignatureFormatOptions = SignatureFormatOptions(),
    stubGenerationOptions: StubGenerationOptions = StubGenerationOptions(),
    internal val apiLevelsGenerationOptions: ApiLevelsGenerationOptions =
        ApiLevelsGenerationOptions(),
) : OptionGroup() {
    /** Writer to direct output to. */
    val stdout: PrintWriter
        get() = executionEnvironment.stdout
    /** Writer to direct error messages to. */
    val stderr: PrintWriter
        get() = executionEnvironment.stderr

    /** Internal list backing [sources] */
    private val mutableSources: MutableList<File> = mutableListOf()
    /** Internal list backing [classpath] */
    private val mutableClassPath: MutableList<File> = mutableListOf()
    /** Internal list backing [mergeQualifierAnnotations] */
    private val mutableMergeQualifierAnnotations: MutableList<File> = mutableListOf()
    /** Internal list backing [mergeInclusionAnnotations] */
    private val mutableMergeInclusionAnnotations: MutableList<File> = mutableListOf()
    /** Internal list backing [passThroughAnnotations] */
    private val mutablePassThroughAnnotations: MutableSet<String> = mutableSetOf()
    /** Internal list backing [excludeAnnotations] */
    private val mutableExcludeAnnotations: MutableSet<String> = mutableSetOf()

    /** API to subtract from signature and stub generation. Corresponds to [ARG_SUBTRACT_API]. */
    var subtractApi: File? = null

    /**
     * Backing property for [nullabilityAnnotationsValidator]
     *
     * This uses [Optional] to wrap the value as [lazy] cannot handle nullable values as it uses
     * `null` as a special value.
     *
     * Creates [NullabilityAnnotationsValidator] lazily as it depends on a number of different
     * options which may be supplied in different orders.
     */
    private val optionalNullabilityAnnotationsValidator by lazy {
        Optional.ofNullable(
            if (validateNullabilityFromMergedStubs || validateNullabilityFromList != null) {
                NullabilityAnnotationsValidator(
                    reporter,
                    nullabilityErrorsFatal,
                    nullabilityWarningsTxt
                )
            } else null
        )
    }

    /** Validator for nullability annotations, if validation is enabled. */
    val nullabilityAnnotationsValidator: NullabilityAnnotationsValidator?
        get() = optionalNullabilityAnnotationsValidator.orElse(null)

    /** Whether nullability validation errors should be considered fatal. */
    private var nullabilityErrorsFatal = true

    /**
     * A file to write non-fatal nullability validation issues to. If null, all issues are treated
     * as fatal or else logged as warnings, depending on the value of [nullabilityErrorsFatal].
     */
    private var nullabilityWarningsTxt: File? = null

    /**
     * Whether to validate nullability for all the classes where we are merging annotations from
     * external java stub files. If true, [nullabilityAnnotationsValidator] must be set.
     */
    var validateNullabilityFromMergedStubs = false

    /**
     * A file containing a list of classes whose nullability annotations should be validated. If
     * set, [nullabilityAnnotationsValidator] must also be set.
     */
    var validateNullabilityFromList: File? = null

    /**
     * Whether to include element documentation (javadoc and KDoc) is in the generated stubs.
     * (Copyright notices are not affected by this, they are always included. Documentation stubs
     * (--doc-stubs) are not affected.)
     */
    private var includeDocumentationInStubs = true

    /**
     * Enhance documentation in various ways, for example auto-generating documentation based on
     * source annotations present in the code. This is implied by --doc-stubs.
     */
    var enhanceDocumentation = false

    /**
     * Whether to allow reading comments If false, any attempts by Metalava to read a PSI comment
     * will return "" This can help callers to be sure that comment-only changes shouldn't affect
     * Metalava output
     */
    var allowReadingComments = true

    /** The list of source roots */
    val sourcePath: List<File> by sourceOptions::sourcePath

    /** The list of dependency jars */
    val classpath: List<File> = mutableClassPath

    /** All source files to parse */
    var sources: List<File> = mutableSources

    /** Lint project description that describes project's module structure in details */
    var projectDescription: File? = null

    val apiClassResolution by
        enumOption(
            help =
                """
                Determines how class resolution is performed when loading API signature files. Any
                classes that cannot be found will be treated as empty.",
            """
                    .trimIndent(),
            enumValueHelpGetter = { it.help },
            default = ApiClassResolution.API_CLASSPATH,
            key = { it.optionValue },
        )

    val allShowAnnotations by apiSelectionOptions::allShowAnnotations

    /**
     * Whether to include unannotated elements if {@link #showAnnotations} is set. Note: This only
     * applies to signature files, not stub files.
     */
    val showUnannotated
        get() = apiSelectionOptions.showUnannotated

    val apiSurfaces
        get() = apiSelectionOptions.apiSurfaces

    /** Packages to include in the API (if null, include all) */
    val apiPackages: PackageFilter? by sourceOptions::apiPackages

    /**
     * An optional [Reportable] predicate that will ignore issues from (i.e. return false for)
     * [Item]s that do not match the [apiPackages] filter. If no [apiPackages] filter is provided
     * then this will be `null`.
     */
    private val reportableFilter: Predicate<Reportable>? by
        lazy(LazyThreadSafetyMode.NONE) {
            apiPackages?.let { packageFilter ->
                Predicate { reportable ->
                    // If we are only emitting some packages (--stub-packages), don't report
                    // issues from other packages
                    (reportable as? Item)?.let { item ->
                        val pkg = (item as? PackageItem) ?: item.containingPackage()
                        pkg == null || packageFilter.matches(pkg)
                    }
                        ?: true
                }
            }
        }

    /** Packages that we should skip generating even if not hidden; typically only used by tests */
    val skipEmitPackages
        get() = executionEnvironment.testEnvironment?.skipEmitPackages ?: emptyList()

    private val annotationManager: AnnotationManager by lazy {
        DefaultAnnotationManager(
            DefaultAnnotationManager.Config(
                passThroughAnnotations = passThroughAnnotations,
                allShowAnnotations = allShowAnnotations,
                showAnnotations = apiSelectionOptions.showAnnotations,
                showSingleAnnotations = apiSelectionOptions.showSingleAnnotations,
                showForStubPurposesAnnotations = apiSelectionOptions.showForStubPurposesAnnotations,
                hideAnnotations = apiSelectionOptions.hideAnnotations,
                suppressCompatibilityMetaAnnotations = suppressCompatibilityMetaAnnotations,
                excludeAnnotations = excludeAnnotations,
                typedefMode = typedefMode,
                apiPredicate = ApiPredicate(config = apiPredicateConfig),
                previouslyReleasedCodebaseProvider = { previouslyReleasedCodebase },
                apiFlags = ApiFlagsCreator.createFromConfig(configFileOptions.config.apiFlags),
            )
        )
    }

    /** Make this available for testing purposes. */
    internal val previouslyReleasedCodebase
        get() = compatibilityCheckOptions.previouslyReleasedCodebase(signatureFileCache)

    internal val codebaseConfig by
        lazy(LazyThreadSafetyMode.NONE) {
            Codebase.Config(
                annotationManager = annotationManager,
                apiSurfaces = apiSurfaces,
                reporter = reporter,
            )
        }

    internal val signatureFileLoader by
        lazy(LazyThreadSafetyMode.NONE) { DefaultSignatureFileLoader(codebaseConfig) }

    internal val signatureFileCache by
        lazy(LazyThreadSafetyMode.NONE) { SignatureFileCache(signatureFileLoader) }

    /** Meta-annotations for which annotated APIs should not be checked for compatibility. */
    private val suppressCompatibilityMetaAnnotations by
        option(
                ARG_SUPPRESS_COMPATIBILITY_META_ANNOTATION,
                help =
                    """
                       Suppress compatibility checks for any elements within the scope of an
                       annotation which is itself annotated with the given meta-annotation.
                    """
                        .trimIndent(),
                metavar = "<meta-annotation class>",
            )
            .multiple()
            .unique()

    /**
     * Whether the generated API can contain classes that are not present in the source but are
     * present on the classpath. Defaults to true for backwards compatibility but is set to false if
     * any API signatures are imported as they must provide a complete set of all classes required
     * but not provided by the generated API.
     *
     * Once all APIs are either self-contained or imported all the required references this will be
     * removed and no classes will be allowed from the classpath JARs.
     */
    private var allowClassesFromClasspath = true

    /** The configuration options for the [ApiAnalyzer] class. */
    val apiAnalyzerConfig by lazy {
        ApiAnalyzer.Config(
            manifest = manifest,
            skipEmitPackages = skipEmitPackages,
            mergeQualifierAnnotations = mergeQualifierAnnotations,
            mergeInclusionAnnotations = mergeInclusionAnnotations,
            allShowAnnotations = allShowAnnotations,
            apiPredicateConfig = apiPredicateConfig,
        )
    }

    val apiPredicateConfig by lazy {
        ApiPredicate.Config(
            ignoreShown = showUnannotated,
            allowClassesFromClasspath = allowClassesFromClasspath,
            addAdditionalOverrides = signatureFileFormat.addAdditionalOverrides,
        )
    }

    /** This is set directly by [preprocessArgv]. */
    private var verbosity: Verbosity = Verbosity.NORMAL

    /** Whether to report warnings and other diagnostics along the way */
    val quiet: Boolean
        get() = verbosity.quiet

    /**
     * Whether to report extra diagnostics along the way (note that verbose isn't the same as not
     * quiet)
     */
    val verbose: Boolean
        get() = verbosity.verbose

    internal val stubWriterConfig by lazy {
        StubWriterConfig(
            includeDocumentationInStubs = includeDocumentationInStubs,
        )
    }

    val stubsDir by stubGenerationOptions::stubsDir
    val forceConvertToWarningNullabilityAnnotations by
        stubGenerationOptions::forceConvertToWarningNullabilityAnnotations
    val generateAnnotations by stubGenerationOptions::includeAnnotations

    /**
     * If set, a directory to write documentation stub files to. Corresponds to the --stubs/-stubs
     * flag.
     */
    var docStubsDir: File? = null

    /** Proguard Keep list file to write */
    var proguard: File? = null

    val apiFile by signatureFileOptions::apiFile
    val removedApiFile by signatureFileOptions::removedApiFile
    val signatureFileFormat by signatureFormatOptions::fileFormat

    /** Path to directory to write SDK values to */
    var sdkValueDir: File? = null

    /**
     * If set, a file to write extracted annotations to. Corresponds to the --extract-annotations
     * flag.
     */
    var externalAnnotations: File? = null

    /** An optional manifest [File]. */
    private val manifestFile by
        option(
                ARG_MANIFEST,
                help =
                    """
        A manifest file, used to check permissions to cross check APIs and retrieve min_sdk_version.
        (default: no manifest)
                    """
                        .trimIndent()
            )
            .file(mustExist = true, canBeDir = false, mustBeReadable = true)

    /**
     * A [Manifest] object to look up available permissions and min_sdk_version.
     *
     * Created lazily to make sure that the [reporter] has been initialized.
     */
    val manifest by lazy { manifestFile?.let { Manifest(it, reporter) } ?: emptyManifest }

    /** The set of annotation classes that should be passed through unchanged */
    private var passThroughAnnotations = mutablePassThroughAnnotations

    /** The set of annotation classes that should be removed from all outputs */
    private var excludeAnnotations = mutableExcludeAnnotations

    /** A signature file to migrate nullness data from */
    val migrateNullsFrom by
        option(
                ARG_MIGRATE_NULLNESS,
                metavar = "<api file>",
                help =
                    """
                        Compare nullness information with the previous stable API
                        and mark newly annotated APIs as under migration.
                    """
                        .trimIndent()
            )
            .existingFile()
            .multiple()
            .map {
                PreviouslyReleasedApi.optionalPreviouslyReleasedApi(
                    ARG_MIGRATE_NULLNESS,
                    it,
                    onlyUseLastForMainApiSurface = false
                )
            }

    /** The list of compatibility checks to run */
    val compatibilityChecks: List<CheckRequest> by compatibilityCheckOptions::compatibilityChecks

    /** The set of annotation classes that should be treated as API compatibility important */
    val apiCompatAnnotations by compatibilityCheckOptions::apiCompatAnnotations

    /** Existing external annotation files to merge in */
    private var mergeQualifierAnnotations: List<File> = mutableMergeQualifierAnnotations
    private var mergeInclusionAnnotations: List<File> = mutableMergeInclusionAnnotations

    val apiVersionLabelProvider: ApiVersionLabelProvider =
        apiLevelsGenerationOptions::getApiVersionLabel

    val includeApiLevelInDocumentation: ApiVersionFilter =
        apiLevelsGenerationOptions::includeApiVersionInDocumentation

    /** Reads API XML file to apply into documentation */
    var applyApiLevelsXml: File? = null

    /** Whether to include the signature file format version header in removed signature files */
    val includeSignatureFormatVersionRemoved: EmitFileHeader
        get() =
            if (deleteEmptyRemovedSignatures) {
                EmitFileHeader.IF_NONEMPTY_FILE
            } else {
                EmitFileHeader.ALWAYS
            }

    var allBaselines: List<Baseline> = emptyList()

    /** [IssueConfiguration] used by all reporters. */
    val issueConfiguration by issueReportingOptions::issueConfiguration

    /** [Reporter] that will redirect [Issues.Issue] depending on their [Issues.Category]. */
    lateinit var reporter: Reporter
        private set

    internal var allReporters: List<DefaultReporter> = emptyList()

    /** If generating a removed signature file, and it is empty, delete it */
    var deleteEmptyRemovedSignatures = false

    /** The language level to use for Java files, set with [ARG_JAVA_SOURCE] */
    var javaLanguageLevelAsString: String = DEFAULT_JAVA_LANGUAGE_LEVEL

    /** The language level to use for Kotlin files, set with [ARG_KOTLIN_SOURCE] */
    var kotlinLanguageLevelAsString: String = DEFAULT_KOTLIN_LANGUAGE_LEVEL

    /**
     * The JDK to use as a platform, if set with [ARG_JDK_HOME]. This is only set when metalava is
     * used for non-Android projects.
     */
    var jdkHome: File? = null

    /**
     * The JDK to use as a platform, if set with [ARG_SDK_HOME]. If this is set along with
     * [ARG_COMPILE_SDK_VERSION], metalava will automatically add the platform's android.jar file to
     * the classpath if it does not already find the android.jar file in the classpath.
     */
    private var sdkHome: File? = null

    /**
     * The compileSdkVersion, set by [ARG_COMPILE_SDK_VERSION]. For example, for R it would be "29".
     * For R preview, it would be "R".
     */
    private var compileSdkVersion: String? = null

    /**
     * How to handle typedef annotations in signature files; corresponds to
     * $ARG_TYPEDEFS_IN_SIGNATURES
     */
    private val typedefMode by
        enumOption(
            ARG_TYPEDEFS_IN_SIGNATURES,
            help = """Whether to include typedef annotations in signature files.""",
            enumValueHelpGetter = { it.help },
            default = TypedefMode.NONE,
            key = { it.optionValue },
        )

    /** Temporary folder to use instead of the JDK default, if any */
    private var tempFolder: File? = null

    var useK2Uast: Boolean? = null

    val sourceModelProvider by
        option(
                ARG_SOURCE_MODEL_PROVIDER,
                hidden = true,
            )
            .choice("psi", "turbine")
            .default("psi")
            .deprecated(
                """WARNING: The turbine model is under work and not usable for now. Eventually this option can be used to set the source model provider to either turbine or psi. The default is psi. """
                    .trimIndent()
            )

    fun parse(args: Array<String>) {
        var index = 0
        while (index < args.size) {
            when (val arg = args[index]) {
                // For now, we don't distinguish between bootclasspath and classpath
                ARG_CLASS_PATH -> {
                    val path = getValue(args, ++index)
                    mutableClassPath.addAll(stringToExistingDirsOrJars(path))
                }
                ARG_SOURCE_FILES -> {
                    val listString = getValue(args, ++index)
                    listString.split(",").forEach { path ->
                        mutableSources.addAll(stringToExistingFiles(path))
                    }
                }
                ARG_SUBTRACT_API -> {
                    if (subtractApi != null) {
                        cliError("Only one $ARG_SUBTRACT_API can be supplied")
                    }
                    subtractApi = stringToExistingFile(getValue(args, ++index))
                }

                // TODO: Remove the legacy --merge-annotations flag once it's no longer used to
                // update P docs
                ARG_MERGE_QUALIFIER_ANNOTATIONS,
                "--merge-zips",
                "--merge-annotations" ->
                    mutableMergeQualifierAnnotations.addAll(
                        stringToExistingDirsOrFiles(getValue(args, ++index))
                    )
                ARG_MERGE_INCLUSION_ANNOTATIONS ->
                    mutableMergeInclusionAnnotations.addAll(
                        stringToExistingDirsOrFiles(getValue(args, ++index))
                    )
                ARG_VALIDATE_NULLABILITY_FROM_MERGED_STUBS -> {
                    validateNullabilityFromMergedStubs = true
                }
                ARG_VALIDATE_NULLABILITY_FROM_LIST -> {
                    validateNullabilityFromList = stringToExistingFile(getValue(args, ++index))
                }
                ARG_NULLABILITY_WARNINGS_TXT ->
                    nullabilityWarningsTxt = stringToNewFile(getValue(args, ++index))
                ARG_NULLABILITY_ERRORS_NON_FATAL -> nullabilityErrorsFatal = false
                ARG_SDK_VALUES -> sdkValueDir = stringToNewDir(getValue(args, ++index))
                ARG_DOC_STUBS -> docStubsDir = stringToNewDir(getValue(args, ++index))
                ARG_EXCLUDE_DOCUMENTATION_FROM_STUBS -> includeDocumentationInStubs = false
                ARG_ENHANCE_DOCUMENTATION -> enhanceDocumentation = true
                ARG_SKIP_READING_COMMENTS -> allowReadingComments = false
                ARG_PASS_THROUGH_ANNOTATION -> {
                    val annotations = getValue(args, ++index)
                    annotations.split(",").forEach { path ->
                        mutablePassThroughAnnotations.add(path)
                    }
                }
                ARG_EXCLUDE_ANNOTATION -> {
                    val annotations = getValue(args, ++index)
                    annotations.split(",").forEach { path -> mutableExcludeAnnotations.add(path) }
                }
                ARG_PROGUARD -> proguard = stringToNewFile(getValue(args, ++index))
                ARG_IGNORE_CLASSES_ON_CLASSPATH -> {
                    allowClassesFromClasspath = false
                }
                ARG_DELETE_EMPTY_REMOVED_SIGNATURES -> deleteEmptyRemovedSignatures = true
                ARG_EXTRACT_ANNOTATIONS ->
                    externalAnnotations = stringToNewFile(getValue(args, ++index))

                // Extracting API levels
                ARG_APPLY_API_LEVELS -> {
                    applyApiLevelsXml =
                        if (apiLevelsGenerationOptions.generateApiLevelXml != null) {
                            // If generating the API file at the same time, it doesn't have
                            // to already exist
                            stringToNewFile(getValue(args, ++index))
                        } else {
                            stringToExistingFile(getValue(args, ++index))
                        }
                }
                ARG_JAVA_SOURCE -> {
                    val value = getValue(args, ++index)
                    javaLanguageLevelAsString = value
                }
                ARG_KOTLIN_SOURCE -> {
                    val value = getValue(args, ++index)
                    kotlinLanguageLevelAsString = value
                }
                ARG_JDK_HOME -> {
                    jdkHome = stringToExistingDir(getValue(args, ++index))
                }
                ARG_SDK_HOME -> {
                    sdkHome = stringToExistingDir(getValue(args, ++index))
                }
                ARG_COMPILE_SDK_VERSION -> {
                    compileSdkVersion = getValue(args, ++index)
                }
                ARG_USE_K2_UAST -> useK2Uast = true
                ARG_PROJECT -> {
                    projectDescription = stringToExistingFile(getValue(args, ++index))
                }
                "--temp-folder" -> {
                    tempFolder = stringToNewOrExistingDir(getValue(args, ++index))
                }

                // Option only meant for tests (not documented); doesn't work in all cases (to do
                // that we'd
                // need JNA to call libc)
                "--pwd" -> {
                    val pwd = stringToExistingDir(getValue(args, ++index)).absoluteFile
                    System.setProperty("user.dir", pwd.path)
                }
                else -> {
                    if (arg.startsWith("-")) {
                        // Some other argument: display usage info and exit
                        throw NoSuchOption(givenName = arg)
                    } else {
                        // All args that don't start with "-" are taken to be filenames
                        mutableSources.addAll(stringToExistingFiles(arg))
                    }
                }
            }

            ++index
        }

        // Initialize the reporters.
        val baseline = generalReportingOptions.baseline
        val reporterUnknown =
            createReporter(
                executionEnvironment = executionEnvironment,
                baseline = baseline,
                errorMessage = null,
            )

        val reporterApiLint =
            createReporter(
                executionEnvironment = executionEnvironment,
                baseline = apiLintOptions.baseline ?: baseline,
                errorMessage = apiLintOptions.errorMessage,
            )

        // [Reporter] for "check-compatibility:*:released".
        // i.e.
        //      [ARG_CHECK_COMPATIBILITY_API_RELEASED] and
        //      [ARG_CHECK_COMPATIBILITY_REMOVED_RELEASED].
        val reporterCompatibilityReleased =
            createReporter(
                executionEnvironment = executionEnvironment,
                baseline = compatibilityCheckOptions.baseline ?: baseline,
                errorMessage = compatibilityCheckOptions.errorMessage,
            )

        // A Reporter that will redirect issues to the appropriate reporter based on the issue's
        // Category.
        reporter =
            CategoryRedirectingReporter(
                defaultReporter = reporterUnknown,
                apiLintReporter = reporterApiLint,
                compatibilityReporter = reporterCompatibilityReleased,
            )

        // Build "all baselines" and "all reporters"

        // Baselines are nullable, so selectively add to the list.
        allBaselines =
            listOfNotNull(baseline, apiLintOptions.baseline, compatibilityCheckOptions.baseline)

        // Reporters are non-null.
        allReporters =
            listOf(
                reporterUnknown,
                reporterApiLint,
                reporterCompatibilityReleased,
            )

        updateClassPath()

        // Make sure that any config files are processed.
        configFileOptions.config
    }

    /**
     * Create a [Reporter] that checks for known issues in [baseline] and prints [errorMessage], if
     * provided, when errors have been reported.
     */
    private fun createReporter(
        executionEnvironment: ExecutionEnvironment,
        baseline: Baseline?,
        errorMessage: String?,
    ) =
        DefaultReporter(
            environment = executionEnvironment.reporterEnvironment,
            issueConfiguration = issueConfiguration,
            baseline = baseline,
            errorMessage = errorMessage,
            reportableFilter = reportableFilter,
            config = issueReportingOptions.reporterConfig,
        )

    /** Update the classpath to insert android.jar or JDK classpath elements if necessary */
    private fun updateClassPath() {
        val sdkHome = sdkHome
        val jdkHome = jdkHome

        if (
            sdkHome != null &&
                compileSdkVersion != null &&
                classpath.none { it.name == FN_FRAMEWORK_LIBRARY }
        ) {
            val jar = File(sdkHome, "platforms/android-$compileSdkVersion")
            if (jar.isFile) {
                mutableClassPath.add(jar)
            } else {
                cliError(
                    "Could not find android.jar for API level $compileSdkVersion in SDK $sdkHome: $jar does not exist"
                )
            }
            if (jdkHome != null) {
                cliError("Do not specify both $ARG_SDK_HOME and $ARG_JDK_HOME")
            }
        } else if (jdkHome != null) {
            val isJre = !isJdkFolder(jdkHome)
            val roots = JavaSdkUtil.getJdkClassesRoots(jdkHome.toPath(), isJre).map { it.toFile() }
            mutableClassPath.addAll(roots)
        }
    }

    private fun getValue(args: Array<String>, index: Int): String {
        if (index >= args.size) {
            cliError("Missing argument for ${args[index - 1]}")
        }
        return args[index]
    }

    private fun stringToExistingDirsOrJars(value: String): List<File> {
        val files = mutableListOf<File>()
        for (path in value.split(File.pathSeparatorChar)) {
            val file = fileForPathInner(path)
            if (!file.isDirectory && !(file.path.endsWith(SdkConstants.DOT_JAR) && file.isFile)) {
                cliError("$file is not a jar or directory")
            }
            files.add(file)
        }
        return files
    }

    private fun stringToExistingDirsOrFiles(value: String): List<File> {
        val files = mutableListOf<File>()
        for (path in value.split(File.pathSeparatorChar)) {
            val file = fileForPathInner(path)
            if (!file.exists()) {
                cliError("$file does not exist")
            }
            files.add(file)
        }
        return files
    }

    @Suppress("unused")
    private fun stringToExistingFileOrDir(value: String): File {
        val file = fileForPathInner(value)
        if (!file.exists()) {
            cliError("$file is not a file or directory")
        }
        return file
    }

    private fun stringToExistingFiles(value: String): List<File> {
        return value
            .split(File.pathSeparatorChar)
            .map { fileForPathInner(it) }
            .map { file ->
                if (!file.isFile) {
                    cliError("$file is not a file")
                }
                file
            }
    }

    private fun stringToNewOrExistingDir(value: String): File {
        val dir = fileForPathInner(value)
        if (!dir.isDirectory) {
            val ok = dir.mkdirs()
            if (!ok) {
                cliError("Could not create $dir")
            }
        }
        return dir
    }
}

object OptionsHelp {
    fun getUsage(terminal: Terminal, width: Int): String {
        val usage = StringWriter()
        val printWriter = PrintWriter(usage)
        usage(printWriter, terminal, width)
        return usage.toString()
    }

    private fun usage(out: PrintWriter, terminal: Terminal, width: Int) {
        val args =
            arrayOf(
                "",
                "API sources:",
                "$ARG_SOURCE_FILES <files>",
                "A comma separated list of source files to be parsed. Can also be " +
                    "@ followed by a path to a text file containing paths to the full set of files to parse.",
                "$ARG_CLASS_PATH <paths>",
                "One or more directories or jars (separated by " +
                    "`${File.pathSeparator}`) containing classes that should be on the classpath when parsing the " +
                    "source files",
                "$ARG_PROJECT <xmlfile>",
                "Project description written in XML according to Lint's project model.",
                "$ARG_MERGE_QUALIFIER_ANNOTATIONS <file>",
                "An external annotations file to merge and overlay " +
                    "the sources, or a directory of such files. Should be used for annotations intended for " +
                    "inclusion in the API to be written out, e.g. nullability. Formats supported are: IntelliJ's " +
                    "external annotations database format, .jar or .zip files containing those, Android signature " +
                    "files, and Java stub files.",
                "$ARG_MERGE_INCLUSION_ANNOTATIONS <file>",
                "An external annotations file to merge and overlay " +
                    "the sources, or a directory of such files. Should be used for annotations which determine " +
                    "inclusion in the API to be written out, i.e. show and hide. The only format supported is " +
                    "Java stub files.",
                ARG_VALIDATE_NULLABILITY_FROM_MERGED_STUBS,
                "Triggers validation of nullability annotations " +
                    "for any class where $ARG_MERGE_QUALIFIER_ANNOTATIONS includes a Java stub file.",
                ARG_VALIDATE_NULLABILITY_FROM_LIST,
                "Triggers validation of nullability annotations " +
                    "for any class listed in the named file (one top-level class per line, # prefix for comment line).",
                "$ARG_NULLABILITY_WARNINGS_TXT <file>",
                "Specifies where to write warnings encountered during " +
                    "validation of nullability annotations. (Does not trigger validation by itself.)",
                ARG_NULLABILITY_ERRORS_NON_FATAL,
                "Specifies that errors encountered during validation of " +
                    "nullability annotations should not be treated as errors. They will be written out to the " +
                    "file specified in $ARG_NULLABILITY_WARNINGS_TXT instead.",
                "$ARG_JAVA_SOURCE <level>",
                "Sets the source level for Java source files; default is $DEFAULT_JAVA_LANGUAGE_LEVEL.",
                "$ARG_KOTLIN_SOURCE <level>",
                "Sets the source level for Kotlin source files; default is $DEFAULT_KOTLIN_LANGUAGE_LEVEL.",
                "$ARG_SDK_HOME <dir>",
                "If set, locate the `android.jar` file from the given Android SDK",
                "$ARG_COMPILE_SDK_VERSION <api>",
                "Use the given API level",
                "$ARG_JDK_HOME <dir>",
                "If set, add the Java APIs from the given JDK to the classpath",
                "$ARG_SUBTRACT_API <api file>",
                "Subtracts the API in the given signature or jar file from the " +
                    "current API being emitted via $ARG_API, $ARG_STUBS, $ARG_DOC_STUBS, etc. " +
                    "Note that the subtraction only applies to classes; it does not subtract members.",
                ARG_IGNORE_CLASSES_ON_CLASSPATH,
                "Prevents references to classes on the classpath from being added to " +
                    "the generated stub files.",
                ARG_SKIP_READING_COMMENTS,
                "Ignore any comments in source files.",
                "",
                "Extracting Signature Files:",
                // TODO: Document --show-annotation!
                "$ARG_PROGUARD <file>",
                "Write a ProGuard keep file for the API",
                "$ARG_SDK_VALUES <dir>",
                "Write SDK values files to the given directory",
                "",
                "Generating Stubs:",
                "$ARG_DOC_STUBS <dir>",
                "Generate documentation stub source files for the API. Documentation stub " +
                    "files are similar to regular stub files, but there are some differences. For example, in " +
                    "the stub files, we'll use special annotations like @RecentlyNonNull instead of @NonNull to " +
                    "indicate that an element is recently marked as non null, whereas in the documentation stubs we'll " +
                    "just list this as @NonNull. Another difference is that @doconly elements are included in " +
                    "documentation stubs, but not regular stubs, etc.",
                "$ARG_PASS_THROUGH_ANNOTATION <annotation classes>",
                "A comma separated list of fully qualified names of " +
                    "annotation classes that must be passed through unchanged.",
                "$ARG_EXCLUDE_ANNOTATION <annotation classes>",
                "A comma separated list of fully qualified names of " +
                    "annotation classes that must be stripped from metalava's outputs.",
                ARG_ENHANCE_DOCUMENTATION,
                "Enhance documentation in various ways, for example auto-generating documentation based on source " +
                    "annotations present in the code. This is implied by --doc-stubs.",
                ARG_EXCLUDE_DOCUMENTATION_FROM_STUBS,
                "Exclude element documentation (javadoc and kdoc) " +
                    "from the generated stubs. (Copyright notices are not affected by this, they are always included. " +
                    "Documentation stubs (--doc-stubs) are not affected.)",
                "",
                "Extracting Annotations:",
                "$ARG_EXTRACT_ANNOTATIONS <zipfile>",
                "Extracts source annotations from the source files and writes " +
                    "them into the given zip file",
                ARG_INCLUDE_SOURCE_RETENTION,
                "If true, include source-retention annotations in the stub files. Does " +
                    "not apply to signature files. Source retention annotations are extracted into the external " +
                    "annotations files instead.",
                "",
                "Injecting API Levels:",
                "$ARG_APPLY_API_LEVELS <api-versions.xml>",
                "Reads an XML file containing API level descriptions " +
                    "and merges the information into the documentation",
                "",
                "Environment Variables:",
                ENV_VAR_METALAVA_DUMP_ARGV,
                "Set to true to have metalava emit all the arguments it was invoked with. " +
                    "Helpful when debugging or reproducing under a debugger what the build system is doing.",
                ENV_VAR_METALAVA_PREPEND_ARGS,
                "One or more arguments (concatenated by space) to insert into the " +
                    "command line, before the documentation flags.",
                ENV_VAR_METALAVA_APPEND_ARGS,
                "One or more arguments (concatenated by space) to append to the " +
                    "end of the command line, after the generate documentation flags."
            )

        val indent = " ".repeat(INDENT_WIDTH)

        var i = 0
        while (i < args.size) {
            val arg = args[i]
            if (arg.isEmpty()) {
                val groupTitle = args[i + 1]
                out.println("\n")
                out.println(terminal.colorize(groupTitle, TerminalColor.YELLOW))
            } else {
                val description = "\n" + args[i + 1]
                val formattedArg = terminal.bold(arg)
                val invisibleChars = formattedArg.length - arg.length
                // +invisibleChars: the extra chars in the above are counted but don't
                // contribute to width so allow more space
                val formatString = "%1$-" + (INDENT_WIDTH + invisibleChars) + "s%2\$s"

                val output =
                    wrap(
                        String.format(formatString, formattedArg, description),
                        width + invisibleChars,
                        width,
                        indent
                    )

                // Remove trailing whitespace
                val lines = output.lines()
                lines.forEachIndexed { index, line ->
                    out.print(line.trimEnd())
                    if (index < lines.size - 1) {
                        out.println()
                    }
                }
            }
            i += 2
        }
    }
}
