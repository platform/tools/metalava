/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tools.metalava.apilevels

/**
 * Represents a specific [ApiVersion] of an API.
 *
 * Supports updating [Api] with information from the [apiVersion] of the API that is defined by
 * this.
 */
sealed class VersionedApi(
    protected val updater: ApiHistoryUpdater,
) {
    /** The [ApiVersion] of the API defined by this. */
    val apiVersion = updater.apiVersion

    /** Update [api] with information from this version of the API. */
    abstract fun updateApi(api: Api)

    /** Provide a string representation for debugging and testing. */
    abstract override fun toString(): String
}
