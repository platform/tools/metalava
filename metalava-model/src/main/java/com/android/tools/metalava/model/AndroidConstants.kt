/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tools.metalava.model

const val ANDROID_ANNOTATION_PACKAGE = "android.annotation"
const val ANDROID_ANNOTATION_PREFIX = "android.annotation."
const val ANDROID_INT_DEF = "android.annotation.IntDef"
const val ANDROID_LONG_DEF = "android.annotation.LongDef"
const val ANDROID_STRING_DEF = "android.annotation.StringDef"

const val ANDROID_SYSTEM_API = "android.annotation.SystemApi"
const val ANDROID_TEST_API = "android.annotation.TestApi"
const val ANDROID_FLAGGED_API = "android.annotation.FlaggedApi"

const val ANDROIDX_ANNOTATION_PACKAGE = "androidx.annotation"
const val ANDROIDX_ANNOTATION_PREFIX = "androidx.annotation."
const val ANDROIDX_INT_DEF = "androidx.annotation.IntDef"
const val ANDROIDX_LONG_DEF = "androidx.annotation.LongDef"
const val ANDROIDX_STRING_DEF = "androidx.annotation.StringDef"

const val ANDROID_NULLABLE = "android.annotation.Nullable"
const val ANDROID_NONNULL = "android.annotation.NonNull"
const val ANDROIDX_NONNULL = "androidx.annotation.NonNull"
const val ANDROIDX_NULLABLE = "androidx.annotation.Nullable"
const val RECENTLY_NULLABLE = "androidx.annotation.RecentlyNullable"
const val RECENTLY_NONNULL = "androidx.annotation.RecentlyNonNull"

const val ANDROID_DEPRECATED_FOR_SDK = "android.annotation.DeprecatedForSdk"

const val ANDROIDX_REQUIRES_PERMISSION = "androidx.annotation.RequiresPermission"
