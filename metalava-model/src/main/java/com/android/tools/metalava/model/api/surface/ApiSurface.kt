/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tools.metalava.model.api.surface

/**
 * A specific API surface.
 *
 * Their natural ordering is determined by the order in which they were created in [surfaces].
 */
sealed interface ApiSurface : Comparable<ApiSurface> {
    /** The set of [ApiSurface]s to which this belongs. */
    val surfaces: ApiSurfaces

    /** The name of the surface. */
    val name: String

    /** The optional [ApiSurface] that this extends. */
    val extends: ApiSurface?

    /** True if this is the main [ApiSurface] being generated. */
    val isMain: Boolean

    /** The list of [ApiVariant]s, in the same order as [ApiVariantType]s. */
    val variants: List<ApiVariant>

    /** The set of all [ApiVariant]s in this [ApiSurface]. */
    val variantSet: ApiVariantSet

    /** Get the [ApiVariant] for [ApiVariantType] in this [ApiSurface]. */
    fun variantFor(type: ApiVariantType): ApiVariant
}
